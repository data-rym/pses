<?php
mb_internal_encoding('utf-8');

//Find dept_numbers here:
define('MAIN_URL', 'http://www.tbs-sct.gc.ca/pses-saff/2008/results-resultats/index-eng.aspx');
define('NBSP', html_entity_decode('&nbsp;', ENT_COMPAT, 'utf-8'));

function get_questions($dept_number = 10) {
  $data = array();
  $rep_content = str_replace(array('&nbsp;', '&ccedil;'), array('&#160;', 'ç')
    , file_get_contents("org-report-$dept_number.html"));
  $xml = @simplexml_load_string($rep_content);
  $xml->registerXPathNamespace('h', 'http://www.w3.org/1999/xhtml');
  $a = $xml->xpath("//h:div[@class='psesquestion' and starts-with(., 'Question ')]");
  foreach ($a as $b) {
    list($q, $t) = explode('. ', $b, 2);
    $data[substr($q, 9)] = $t;
  }
  ksort($data);
  return $data;
}

function get_all_dept_data($question_number, $keep_all = true, $skip_last = 0) {
  static $cache;

  if (empty($cache[$question_number][$keep_all][$skip_last])) {
    $depts = array();
    foreach (glob('org-report-??.html') as $d) {
      list(,,$dept) = explode('-', basename($d, '.html'));
      $depts[$dept] = get_dept_data($dept, $question_number, $keep_all, $skip_last);
    }

    $cache[$question_number][$keep_all][$skip_last] = $depts;
  }
  return $cache[$question_number][$keep_all][$skip_last];
}

function get_row_headers($question_number) {
  $d = array();
  foreach (get_all_dept_data($question_number, false) as $x) {
    list($d[]) = array_keys($x->responses);
  }
  return $d;
}

function get_dept_column_headers($dept_number, $question_number, $skip_last = 0) {
  $x = get_dept_data($dept_number, $question_number, false, $skip_last);
  return array_keys(array_pop($x->responses));
}

function get_dept_data($dept_number, $question_number, $keep_all = true, $skip_last = 0) {
  static $cache;

  if (!isset($cache[$dept_number][$question_number][$keep_all][$skip_last])) {
    $rep_content = str_replace(array('&nbsp;', '&ccedil;'), array('&#160;', 'ç')
      , file_get_contents("org-report-$dept_number.html"));
    $xml = @simplexml_load_string($rep_content);
    $xml->registerXPathNamespace('h', 'http://www.w3.org/1999/xhtml');
    $p1 = "//h:div[@class='psesquestion' and starts-with(., 'Question $question_number.')]";
    $p2 = "following-sibling::h:table[@class='answertable' and contains(@summary, 'question $question_number ')]";
    $a = $xml->xpath("$p1/$p2");
    $answer_names = array();
    $answers = new StdClass;
    if ($a) {
      //$answers->summary = (string)$a[0]['summary'];
      $answers->source = 'http://www.tbs-sct.gc.ca/pses-saff/2008/results-resultats/res-eng.aspx?o1=' . $dept_number;
      foreach ($a[0]->tr as $n=>$row) {
        if (isset($row->th)) {
          if (count($answer_names)) $index = (string)$row->th;
          else foreach ($row->th as $n1=>$d) if (trim($d, NBSP)) $answer_names[] = (string)$d;
        }
        if (isset($row->td) && ($keep_all || ('Public Service of Canada' !== $index))) {
          $i = 0;
          foreach ($row->td as $n2=>$d) $answers->responses[$index][$answer_names[$i++]] = (string)$d;
            if (++$skip_last) while ($skip_last--) array_pop($answers->responses[$index]);
        }
      }
    }
    $cache[$dept_number][$question_number][$keep_all][$skip_last] = $answers;
  }
  return $cache[$dept_number][$question_number][$keep_all][$skip_last];
}

function cache_all() {
  $fc = file_get_contents(MAIN_URL);
  preg_match_all('/"units-unites-eng\.aspx\?o1=(\d+)"/', $fc, $matches);
  foreach ($matches[1] as $m) cache_one($m);
}

function cache_one($dept_number) {
  $in = 'http://www.tbs-sct.gc.ca/pses-saff/2008/results-resultats/res-eng.aspx?o1=' . $dept_number;
  $out = "org-report-$dept_number.html";
  if (!file_exists($out) && !copy($in, $out)) {
    $in_c = file_get_contents($in);
    if (!file_put_contents($out, $in_c)) die ('oh well!');
  }
}

