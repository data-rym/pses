<?php
require 'pses.inc.php';
header('Content-Type: text/javascript');
$q = isset($_GET['q']) ? $_GET['q'] : '71';
$s = isset($_GET['s']) ? $_GET['s'] : 0;
$col_headers = get_dept_column_headers(10, $q, $s);
$n_col_headers = count($col_headers);
$row_headers = get_row_headers($q);
$n_row_headers = count($row_headers);
$data = get_all_dept_data($q, false, $s);
?>

google.load('visualization', 1);
google.setOnLoadCallback(drawVisualization);

function drawVisualization() {
  var rowHeaderCount = 1;
  var columnHeaderCount = 1;
  var rows = <?php echo $n_row_headers ?> + rowHeaderCount;
  var columns = <?php echo $n_col_headers ?> + columnHeaderCount;
  var data = new google.visualization.DataTable();
  var i = columns - 1;
  data.addColumn('string');
  do
  {
    data.addColumn('string');
  }
  while (i-- > 1)

  data.addRows(rows);
  setRowHeaders(data);
  setColumnHeaders(data);
  setData(data);
  var vis = new greg.ross.visualisation.MagicTable(document.getElementById('chart_div'));
  options = {};
  options.tableTitle = "Survey Chart";
  options.enableFisheye = true;
  options.enableBarFill = true;
  options.defaultRowHeight = 25;
  options.defaultColumnWidth = 70;
  options.columnWidths = [{column : 0, width : 130}];
  options.rowHeaderCount = rowHeaderCount;
  options.columnHeaderCount = columnHeaderCount;
  options.tablePositionX = 0;
  options.tablePositionY = 0;
  options.tableHeight = 400;
  options.tableWidth = 600;
  options.colourRamp = getColourRamp();
  vis.draw(data, options);
}

function getColourRamp() {
  var colour1 = {red:0, green:0, blue:255};
  var colour2 = {red:0, green:255, blue:255};
  var colour3 = {red:0, green:255, blue:0};
  var colour4 = {red:255, green:255, blue:0};
  var colour5 = {red:255, green:0, blue:0};
  return [colour1, colour2, colour3, colour4, colour5];
}

function setRowHeaders(data) {
  data.setCell(0, 0, 'dept/response');
  <?php foreach ($row_headers as $n => $r) : ?>
  data.setCell(<?php echo $n+1 ?>, 0, "<?php echo $r ?>");
  <?php endforeach ?>
}

function setColumnHeaders(data) {
  <?php foreach ($col_headers as $n => $r) : ?>
  data.setCell(0, <?php echo $n+1 ?>, "<?php echo $r ?>");
  <?php endforeach ?>
}

function setData(data) {
  <?php $x = 0; foreach ($data as $d): ?>
  <?php $y = 0; ++$x; $a = array_pop($d->responses); foreach ($a as $v): ?>
  data.setCell(<?php echo $x ?>, <?php echo ++$y ?>, <?php echo $v ?> + "");
  <?php endforeach ?>
  <?php endforeach ?>
}

