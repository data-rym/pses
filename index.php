<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <title>2008 Public Service Employee Survey Visualization</title>
    <script type='text/javascript' src='http://magic-table.googlecode.com/svn/trunk/magic-table/javascript/magic_table.js'></script>
    <script type='text/javascript' src='http://www.google.com/jsapi'></script>
  </head>
  <body>
  <h1><a href='.'>2008 Public Service Employee Survey</a></h1>
  <?php require 'pses.inc.php'; if (empty($_GET['q'])): ?>
  <h2>Questions</h2>
  <ul>
  <?php foreach (get_questions() as $q => $v): ?>
  <li><a href='?q=<?php echo $q ?>'><?php echo "$q." ?></a> <?php echo $v ?></li>
  <?php endforeach ?>
  </ul>
  <?php else: ?>
  <h2>Question <?php echo $_GET['q'] ?></h2>
  <h3>Data Visualization</h3>
  <p><?php $qs = get_questions(); echo $qs[$_GET['q']]?></p>
  <script type='text/javascript' src='table.js.php?q=<?php echo $_GET['q'];
    if (isset($_GET['s'])) echo '&s=' . $_GET['s'] ?>'></script>
  <form method='GET' action='.'>
  Skip last X columns: <input type='text' name='s' size='5' value='<?php echo isset($_GET['s']) ? $_GET['s'] : 4 ?>' />
  <input type='hidden' name='q' value='<?php echo $_GET['q'] ?>' />
  <input type='submit' />
  </form>
  <div id="chart_div"></div>
  <?php endif ?>
  <address><a href='http://rym.waglo.com/'>Robin Millette</a> - <a href='.'>main</a>
    - <a href='http://gitorious.org/data-rym/pses'>source code</a> - <a href='<?php echo MAIN_URL ?>'>data</a></address>
  </body>
</html>
