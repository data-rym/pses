<?php
require 'pses.inc.php';

// Usage: http://example.com/json?dept=82&question=78

$dept_number = isset($_GET['dept']) ? $_GET['dept'] : '82';
$question_number = isset($_GET['question']) ? $_GET['question'] : '71';
cache_one($dept_number);
header('Content-Type: application/json');
echo json_encode(get_dept_data($dept_number, $question_number));

